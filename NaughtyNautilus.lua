--[[
	Name: NaughtyNautilus
	Author: TurtleBot
	Version: 100
	Credits: Pain.
	Features;
		Spacebar To Win

--]]
local version = 100
local autoUpdate = true
local SilentPrint = false --[[Change to true if you wish the script to not print anything in chat. Does not work for Libs or Errors]]

if not VIP_USER or myHero.charName ~= "Nautilus" then return end
--[[Credits to Hellsing, QQQ and Honda7]]
local host = "bitbucket.org"
local path = "/turtlesoft/bol/raw/master/NaughtyNautilus.lua".."?rand="..math.random(1,10000)
local url  = "https://"..host..path
local printMessage = function(message) if not SilentPrint then print("<font color=\"#6699ff\"><b>NaughtyNautilus:</b></font> <font color=\"#FFFFFF\">" .. message .. "</font>") end end
local webResult = GetWebResult(host, path)
if autoUpdate then
	if webResult then
		local serverVersion = string.match(webResult, "%s*local%s+version%s+=%s+.*%d+%.%d+")
		if serverVersion then
			serverVersion = tonumber(string.match(serverVersion, "%d+%.?%d*"))
			if version < serverVersion then
				printMessage("New version available: v" .. serverVersion)
				printMessage("Updating, please don't press F9")
				DelayAction(function () DownloadFile(url, SCRIPT_PATH .. GetCurrentEnv().FILE_NAME, function () printMessage("Successfully updated, please reload!") end) end, 2)
			else
				printMessage("You've got the latest version: v" .. serverVersion)
			end
		else
			printMessage("Something went wrong! Please manually update the script!")
		end
	else
		printMessage("Error downloading version info!")
	end
else
	printMessage("Auto Updater has been disabled, please enable it to automatically keep up to date!")
end
local REQUIRED_LIBS = {
	["VPrediction"] = "https://raw.githubusercontent.com/honda7/BoL/master/Common/VPrediction.lua",
	["SOW"] = "https://raw.githubusercontent.com/honda7/BoL/master/Common/SOW.lua"
}               
local DOWNLOADING_LIBS = false
local DOWNLOAD_COUNT = 0
local SELF_NAME = GetCurrentEnv() and GetCurrentEnv().FILE_NAME or ""
function AfterDownload()
	DOWNLOAD_COUNT = DOWNLOAD_COUNT - 1
	if DOWNLOAD_COUNT == 0 then
		DOWNLOADING_LIBS = false
		printMessage("Required libraries downloaded successfully, please reload (double [F9]).</font>")
	end
end 
for DOWNLOAD_LIB_NAME, DOWNLOAD_LIB_URL in pairs(REQUIRED_LIBS) do
	if FileExist(LIB_PATH .. DOWNLOAD_LIB_NAME .. ".lua") then
		require(DOWNLOAD_LIB_NAME)
	else
		DOWNLOADING_LIBS = true
		DOWNLOAD_COUNT = DOWNLOAD_COUNT + 1
		printMessage("Not all required libraries are installed. Downloading: <b><u><font color=\"#73B9FF\">"..DOWNLOAD_LIB_NAME.."</font></u></b> now! Please don't press [F9]!</font>")
		DownloadFile(DOWNLOAD_LIB_URL, LIB_PATH .. DOWNLOAD_LIB_NAME..".lua", AfterDownload)
	end
end 
if DOWNLOADING_LIBS then return end
--[[End of Credits]]


local Nautilus = {
	Q = {range = 1100, speed = 1200, delay = 0.5, width = 80, collision = true, hitchance = 2},
	W = {range = math.huge, speed = math.huge, delay = 0.5, width = math.huge, collision = false, hitchance = 1},
	E = {range = 600, speed = math.huge, delay = 0.5, width = math.huge, collision = false, hitchance = 1},
	R = {range = 825, speed = 1400, delay = 0.5, width = 80, collision = false, hitchance = 2}
}
function OnLoad()
	QCast = false
	VP = VPrediction(true) --[[Loads VPrediction]]
	Menu = scriptConfig("Nautilus","Nautilus") --[[Starts the Shift Menu]]
	Menu:addParam("Author","Author: TurtleBot",5,"")
	Menu:addParam("Version","Version: "..version,5,"")
	Menu.Thread = false
	Menu:addSubMenu("Nautilus: Key Bindings","General")
		Menu.General:addParam("Combo","Combo",2,false,32)
		Menu.General:addParam("DredgeLine","Dredge Line",2,false,string.byte("v"))
	Menu:addSubMenu("Nautilus: Combo","Combo")
		Menu.Combo:addParam("Q","Use Q in 'Combo'",1,true)
		Menu.Combo:addParam("W","Use W in 'Combo'",1,true)
		Menu.Combo:addParam("E","Use E in 'Combo'",1,true)
		Menu.Combo:addParam("R","Use R in 'Combo'",1,true)
	Menu:addSubMenu("Nautilus: Hit Chance","HC")
		Menu.HC:addParam("Q","Cast Q if:",7,Nautilus.Q["hitchance"], { "Low Hit Chance", "High Hit Chance", "Target Slow/Close", "Target Immobilised", "Target Dashing/Blinking"})
		Menu.HC:addParam("E","Cast E if:",7,Nautilus.E["hitchance"], { "Low Hit Chance", "High Hit Chance", "Target Slow/Close", "Target Immobilised"})
	Menu:addSubMenu("Nautilus: Extra","Extra")
		Menu.Extra:addParam("Tower","Don't Dive Towers with Q",1,true)
	Menu:addSubMenu("Nautilus: Drawing","Draw")
		Menu.Draw:addParam("Q","Draw Q range",1,true)
		Menu.Draw:addParam("W","Draw W range",1,true)
		Menu.Draw:addParam("E","Draw E range",1,true)
		Menu.Draw:addParam("R","Draw R range",1,true)
		Menu.Draw:addParam("Combo","Show 'Combo'",1,true)
	if Menu.Draw.Combo then
		Menu.General:permaShow("Combo")
	end
	ts = TargetSelector(8,1100,1,false) --[[AllClass target selector]]
	ts.name = "Nautilus Target"
	Menu:addTS(ts)
	printMessage("Script Loaded")
	if not _G.SOWLoaded then
		SOWi = SOW(VP)
		SMenu = scriptConfig("Simple Orbwalker", "Simple Orbwalker")
		SMenu:addSubMenu("Drawing", "Drawing")
		SMenu.Drawing:addParam("Range", "Draw auto-attack range", SCRIPT_PARAM_ONOFF, true)
		SOWi:LoadToMenu(SMenu)
		SOWi:RegisterAfterAttackCallback(AfterAttack)
	end
	MenuLoad = true
end

function OnTick()
	if MenuLoad ~= true then return end
	ts:update()
	Target = ts.target
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)

	if Target then
		if Menu.General.Combo then
			if Menu.Combo.Q and QREADY and not QCast then
				local CastPosition, HitChance, Position = VP:GetLineCastPosition(Target,Nautilus.Q["delay"],Nautilus.Q["width"],Nautilus.Q["range"],Nautilus.Q["speed"],myHero,Nautilus.Q["collision"])
				if GetDistance(myHero,CastPosition) <= Nautilus.Q["range"] and HitChance >= Menu.HC.Q and HitChance ~= -1 then
					CastSpell(_Q,CastPosition.x,CastPosition.z)
				end
			end

			if Menu.Combo.W and WREADY then
				CastSpell(_W, myHero)
			end
			
			if Menu.Combo.E and EREADY and not QCast then
				if EREADY and Menu.General.Combo and Target and not QCast then 
					local CastPosition, HitChance, nTargets = VP:GetLineAOECastPosition(Target,Nautilus.E["delay"],Nautilus.E["width"],Nautilus.E["range"],Nautilus.E["speed"],myHero,Nautilus.E["collision"])
					if HitChance >= Menu.HC.E or HitChance == 0 then				
						local CastPosition = Vector(myHero.x + (myHero.x - CastPosition.x),myHero.y,myHero.z + (myHero.z - CastPosition.z))
						if GetDistance(myHero,CastPosition) <= Nautilus.E["range"] then
							CastSpell(_E,CastPosition.x,CastPosition.z)
						end
					end
				end
			end

			if Menu.Combo.R and RREADY then
				if GetDistance(myHero,CastPosition) <= Nautilus.R["range"] + 75 and GetDistance(myHero,CastPosition) <= Nautilus.R["range"] - 75 then
					CastSpell(_R, Target)
				end
			end
		end
	end
end

function OnDraw()
	if SOWi and SMenu.Drawing.Range then
        SOWi:DrawAARange()
    end
	if Menu.Draw.Q and QREADY then DrawCircle(myHero.x,myHero.y,myHero.z,Nautilus.Q["range"],0x00FFFF) end
	if Menu.Draw.W and WREADY then DrawCircle(myHero.x,myHero.y,myHero.z,Nautilus.W["range"],0x00FFFF) end
	if Menu.Draw.E and EREADY then DrawCircle(myHero.x,myHero.y,myHero.z,Nautilus.E["range"],0x00FFFF) end
	if Menu.Draw.R and RREADY then DrawCircle(myHero.x,myHero.y,myHero.z,Nautilus.R["range"],0x00FFFF) end
end

function TargetUnderTower(unit)
	for k = 1, objManager.maxObjects do
		local obj = objManager:GetObject(k)
		if obj ~= nil then
			if obj.name:find("Turret_T2") and obj.name:find("_A") and obj.team ~= myHero.team and GetDistance(unit,obj) <= 825 then
				return true
			end
		end
	end
	return false
end
